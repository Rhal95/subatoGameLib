package name.panitz.game.framework.object;

import name.panitz.game.framework.Vertex;

public interface Sizeable {
  double getHeight();

  double getWidth();

  default Vertex getSize() {
    return new Vertex(getWidth(), getHeight());
  }
}
