package name.panitz.game.framework.object;

import name.panitz.game.framework.Vertex;

public abstract class AbstractGameObject implements GameObject, Sizeable {
  /*+ Ein Spielobjekt benötigt Felder für die in der Schnittstelle <tt>GameObject</tt> verlangten Getter- und Setter-Methoden.*/
  double width;
  double height;
  Vertex position;
  Vertex velocity;

  /*+ Mehrere Kosntruktoren stehen bereit, diese Felder zu initialisieren. */

  protected AbstractGameObject(double width, double height, Vertex position, Vertex velocity) {
    this.width = width;
    this.height = height;
    this.position = position;
    this.velocity = velocity;
  }

  protected AbstractGameObject(double width, double height, Vertex position) {
    this(width, height, position, new Vertex(0, 0));
  }

  protected AbstractGameObject(double width, double height) {
    this(width, height, new Vertex(0, 0));
  }

  protected AbstractGameObject(double widthAndHeight) {
    this(widthAndHeight, widthAndHeight);
  }

  /*+ Die Getter- und Setter-Methoden er Schnittstelle. */

  @Override
  public double getWidth() {
    return width;
  }

  @Override
  public void setWidth(double w) {
    width = w;
  }

  @Override
  public double getHeight() {
    return height;
  }

  @Override
  public void setHeight(double h) {
    height = h;
  }

  @Override
  public Vertex getPosition() {
    return position;
  }

  @Override
  public void setPosition(Vertex v) {
    position = v;
  }

  @Override
  public Vertex getVelocity() {
    return velocity;
  }

  @Override
  public void setVelocity(Vertex v) {
    velocity = v;
  }
}

