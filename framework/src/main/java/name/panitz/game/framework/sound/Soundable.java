package name.panitz.game.framework.sound;

/**
 * Interface representing a sound that can be played on a {@link SoundTool}.
 */
public interface Soundable {
  /**
   * Retrieves the name of this sound object.
   *
   * @return the sound of this object
   */
  String getSound();
}
