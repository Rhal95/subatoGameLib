package name.panitz.game.framework.input;

public enum ButtonCode {
  NONE, LEFT_MOUSE, RIGHT_MOUSE, MIDDLE_MOUSE, WHEEL_UP, WHEEL_DOWN
}
