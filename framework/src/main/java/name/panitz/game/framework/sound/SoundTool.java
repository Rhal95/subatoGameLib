package name.panitz.game.framework.sound;


import java.util.List;

/**
 * Interface abstracting a sound device.
 * Sounds are represented using strings without any format requirement.
 * The implementation needs to specify the format that is used.
 *
 * Possible implementations:
 * - File based sounds (file is part of the project)
 * - URI based sounds (possible remote file uri, web uri, ...)
 * - Name based sounds (Play one of n pre-defined sounds)
 * - ...
 */
public interface SoundTool {
  /**
   * Dummy implementation if no sounds are used.
   */
  SoundTool NO_SOUND = new SoundTool() {
    @Override
    public Object loadSound(String sound) {
      throw new UnsupportedOperationException("SoundTool.NO_SOUND.loadSound() is not implemented intentionally.");
    }
    @Override
    public void playSound(String sound) {
      // do nothing
    }
  };

  /**
   * Loads the sound specified by the given string.
   * @param sound the name of the sound to load.
   * @return an object representing the loaded sound.
   */
  Object loadSound(String sound);

  /**
   * Plays the sound specified by the given string.
   * @param sound the sound to play
   */
  void playSound(String sound);

  /**
   * Play all sounds in the provided list.
   *
   * @param soundsToPlay list of the sounds to play.
   */
  default void playSounds(List<Soundable> soundsToPlay) {
    soundsToPlay.stream().map(Soundable::getSound).forEach(this::playSound);
  }
}
