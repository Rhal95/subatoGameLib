package name.panitz.game.framework.input;

import name.panitz.game.framework.Vertex;

import java.util.Objects;

public final class MouseInput implements Input {
  private final Vertex position;
  private final ButtonCode mouseButton;
  private final ButtonStatus status;

  MouseInput(Vertex position,
             ButtonCode mouseButton,
             ButtonStatus status) {
    this.position = position;
    this.mouseButton = mouseButton;
    this.status = status;
  }

  public Vertex position() {
    return position;
  }

  public ButtonCode mouseButton() {
    return mouseButton;
  }

  public ButtonStatus status() {
    return status;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) return true;
    if (obj == null || obj.getClass() != this.getClass()) return false;
    var that = (MouseInput) obj;
    return Objects.equals(this.position, that.position) &&
               Objects.equals(this.mouseButton, that.mouseButton) &&
               Objects.equals(this.status, that.status);
  }

  @Override
  public int hashCode() {
    return Objects.hash(position, mouseButton, status);
  }

  public static MouseInput of(Vertex position, ButtonCode mouseButton, ButtonStatus status) {
    return new MouseInput(position, mouseButton, status);
  }

  @Override
  public String toString() {
    return "MouseInput[" +
               "position=" + position + ", " +
               "mouseButton=" + mouseButton + ", " +
               "status=" + status + ']';
  }
}
