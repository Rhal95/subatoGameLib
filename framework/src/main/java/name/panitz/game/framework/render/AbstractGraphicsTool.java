package name.panitz.game.framework.render;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import name.panitz.game.framework.Vertex;

public abstract class AbstractGraphicsTool<I> implements GraphicsTool {
  private final Map<String, I> imageMap = new HashMap<>();

  public Vertex getImageSize(String imageFileName) {
    return getImage(imageFileName)
               .map(this::getSizes)
               .orElse(new Vertex(0, 0));
  }

  protected abstract Vertex getSizes(I image);

  protected abstract I loadImage(String name);

  public Optional<I> getImage(String name) {
    return Optional.ofNullable(imageMap.get(name))
               .or(() -> {
                 imageMap.put(name, loadImage(name));
                 return Optional.of(imageMap.get(name));
               });
  }

}
