package name.panitz.game.framework.input;

import name.panitz.game.framework.logic.InputState;

public interface GameInput<I extends InputState<I>> {
  I getInput(I input);
}
