package name.panitz.game.framework.object;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.render.GraphicsTool;

public class TextObject extends AbstractGameObject {
  public String text;
  public String fontName;
  public int fontSize;

  public TextObject(Vertex position, String text, String fntName, int fntSize) {
    super(0, 0, position);
    this.text = text;
    this.fontName = fntName;
    this.fontSize = fntSize;
  }

  @Override
  public void paintTo(GraphicsTool g) {
    g.drawString(getPosition().x(), getPosition().y(), fontSize, fontName, text);
  }
}

