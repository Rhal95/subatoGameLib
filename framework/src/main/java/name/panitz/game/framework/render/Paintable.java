package name.panitz.game.framework.render;

public interface Paintable {
  void paintTo(GraphicsTool g);
}

