package name.panitz.game.framework.logic;

import java.util.Collections;
import java.util.List;
import name.panitz.game.framework.input.Input;
import name.panitz.game.framework.input.KeyInput;
import name.panitz.game.framework.input.MouseInput;
import name.panitz.game.framework.render.Paintable;

public abstract class AbstractGame<G extends AbstractGame<G>> implements GameLogic<G, G>, GameState, InputState<G> {

  public abstract void calculateTick(double deltaTime);

  @Override
  public G calculateTick(G gameState, G input, double deltaTime) {
    calculateTick(deltaTime);
    return (G) this;
  }

  @Override
  public G createInitialState() {
    return (G) this;
  }

  @Override
  public G createInitialInput() {
    return (G) this;
  }

  public abstract void handleMouseInput(MouseInput input);

  public abstract void handleKeyInput(KeyInput input);

  @Override
  public G handleInput(Input input) {
    if (input instanceof KeyInput) {
      var keyInput = (KeyInput) input;
      handleKeyInput(keyInput);
    } else if (input instanceof MouseInput) {
      var mouseInput = (MouseInput) input;
      handleMouseInput(mouseInput);
    }
    return (G) this;
  }

  @Override
  public List<Paintable> getAllObjectsToPaint() {
    return Collections.unmodifiableList(getAllGameObjects());
  }
}

