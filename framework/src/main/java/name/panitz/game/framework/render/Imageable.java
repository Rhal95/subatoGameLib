package name.panitz.game.framework.render;

import name.panitz.game.framework.object.Positionable;
import name.panitz.game.framework.object.Sizeable;

public interface Imageable extends Paintable, Positionable, Sizeable {
  String getImagePath();

  @Override
  default void paintTo(GraphicsTool canvas) {
    canvas.drawImage(getImagePath(), getPosition().x(), getPosition().y());
  }
}
