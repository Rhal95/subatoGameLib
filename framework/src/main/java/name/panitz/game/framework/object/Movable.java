package name.panitz.game.framework.object;

import name.panitz.game.framework.Vertex;

public interface Movable extends Positionable {
  Vertex getVelocity();

  void setVelocity(Vertex position);

  void setPosition(Vertex position);

  default void setVelocityX(int x) {
    setPosition(new Vertex(x, getPosition().y()));
  }

  default void setVelocityY(int y) {
    setPosition(new Vertex(getPosition().x(), y));
  }

  default void move() {
    setPosition(getPosition().add(getVelocity()));
  }

  default void setPositionX(int x) {
    setPosition(new Vertex(x, getPosition().y()));
  }

  default void setPositionY(int y) {
    setPosition(new Vertex(getPosition().x(), y));
  }
}
