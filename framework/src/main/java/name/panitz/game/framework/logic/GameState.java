package name.panitz.game.framework.logic;

import java.io.Serializable;
import java.util.List;
import name.panitz.game.framework.GameStatus;
import name.panitz.game.framework.object.GameObject;
import name.panitz.game.framework.render.Paintable;
import name.panitz.game.framework.sound.Soundable;

public interface GameState extends Serializable {
  /**
   * The current status of the game.
   *
   * @return The status of the game.
   */
  GameStatus getStatus();

  /**
   * Returns all game objects in the game state.
   *
   * @return an Iterable with all GameObjects stored in the state.
   */
  List<GameObject> getAllGameObjects();

  /**
   * Returns all Objects that should be painted this tick. The order of the Iterable determines in which order the
   * elements will be rendered.
   *
   * @return list of the objects to render.
   */
  List<Paintable> getAllObjectsToPaint();

  /**
   * @return All sounds that should be played in the current GameState
   */
  List<Soundable> getAllSoundsToPlay();
}
