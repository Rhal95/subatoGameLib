package name.panitz.game.framework.input;

import name.panitz.game.framework.Vertex;

public interface InputMapper<K, M> {

  KeyCode toKeyCode(K key);

  ButtonCode toButtonCode(M mouse);

  Vertex toMousePosition(M mouse);
}
