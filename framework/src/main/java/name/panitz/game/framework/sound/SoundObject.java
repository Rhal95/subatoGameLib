package name.panitz.game.framework.sound;

/**
 * Object representing a sound.
 */
public class SoundObject implements Soundable {
  private final String sound;

  /**
   * Create a new sound object with the given string as name.
   *
   * @param sound the name of the sound
   */
  public SoundObject(String sound) {
    this.sound = sound;
  }

  public String getSound() {
    return sound;
  }
}
