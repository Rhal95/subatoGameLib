package name.panitz.game.framework.logic;

import java.io.Serializable;
import name.panitz.game.framework.input.Input;

public interface InputState<I extends InputState<I>> extends Serializable {
  I handleInput(Input input);
}
