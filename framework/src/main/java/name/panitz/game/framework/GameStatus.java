package name.panitz.game.framework;

/**
 * Enum indicating the current game state.
 */
public enum GameStatus {
  /**
   * The game is not yet started. It may already be initialized.
   */
  NOT_STARTED,
  /**
   * The game is currently running.
   */
  RUNNING,
  /**
   * The game is currently paused. It may resume to {@code RUNNING} at any time.
   */
  PAUSED,
  /**
   * The player has lost the game. It may not change state again.
   */
  LOST,
  /**
   * The player has won the game. It may not change state again.
   */
  WON;

  /**
   * Convenience method to determine if the game is over.
   *
   * @return if the game is over
   */
  public boolean isOver() {
    return this == LOST || this == WON;
  }
}
