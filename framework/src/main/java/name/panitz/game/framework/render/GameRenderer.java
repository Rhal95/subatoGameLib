package name.panitz.game.framework.render;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.logic.GameState;

public interface GameRenderer {
  void renderState(GameState state);

  void setSize(Vertex size);
}
