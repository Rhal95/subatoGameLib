package name.panitz.game.framework.object;

/*+ Die Eigenschaften einer Spielfigur werden durch eine Schnittstelle definiert. */

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.render.Paintable;

public interface GameObject extends Movable, Paintable {
  /*+ Zunächst gibt es Getter- und Setter-Methoden, wir die Weite und Höhe einer Spielfigur. */

  double getWidth();

  void setWidth(double w);

  double getHeight();

  void setHeight(double h);

  /*+ Es lässt sich die aktuelle Position auf dem Spielfeld erfragen. */

  /*+ Der Bewegungsvektor ist zu erfragen und zu setzen. */

  Vertex getVelocity();

  void setVelocity(Vertex v);

  default void accelerate(Vertex v) {
    setVelocity(getVelocity().add(v));
  }

  default void accelerate(double m) {
    setVelocity(getVelocity().mult(m));
  }

  default void moveBy(Vertex v) {
    setPosition(getPosition().add(v));
  }

    /*+ Weitere Methoden lassen sich bereits in dieser Schnittstelle las default-Methoden implementieren.

      Ist das Objekt links von einem anderen, ohne es zu berühren? */

  default boolean isLeftOf(GameObject that) {
    return this.getPosition().x() + this.getWidth() < that.getPosition().x();
  }
  /*+ Damit lässt sich auch direkt erfragen, ob  das Objekt rechts von einem anderen ist , ohne es zu berühren? */

  default boolean isRightOf(GameObject that) {
    return that.isLeftOf(this);
  }

  /*+ Was in der Horizontalen geht, ist analog in der Vertikalen zu erfragen.*/

  default boolean isAbove(GameObject that) {
    return this.getPosition().y() + this.getHeight() < that.getPosition().y();
  }

  /*+ Und damit ist leicht definiert, ob sich zwei Objekte berühren. */

  default boolean touches(GameObject that) {
    return !this.isLeftOf(that) &&
               !that.isLeftOf(this) &&
               !this.isAbove(that) &&
               !that.isAbove(this);
  }

    /*+ Wenn man mit Schwerkraft und Böden arbeitet, ist es oft gut zu wissen,
      ob eine Spielfigur genau auf einer anderen steht. */

  default boolean isStandingOnTopOf(GameObject that) {
    return !(isLeftOf(that) || isRightOf(that)) && isAbove(that)
               && getPosition().y() + getHeight() + 2 > that.getPosition().y();
  }

  default void move() {
    setPosition(getPosition().add(getVelocity()));
  }

  default double size() {
    return getWidth() * getHeight();
  }

  default boolean isLargerThan(GameObject that) {
    return size() > that.size();
  }
}

