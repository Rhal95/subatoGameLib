package name.panitz.game.framework.clock;

/**
 * Simple implementation for a {@code GameTimer}. The provided {@code SimpleClock.SLOW_CLOCK}, {@code
 * SimpleClock.MEDIUM_CLOCK} and {@code SimpleClock.FAST_CLOCK} can be used as fixed 30, 45 and 60 Hz clocks
 * respectively. Only use one clock instance for one game.
 */
public class SimpleClock implements GameTimer {
  /** A 30 Hz clock instance */
  public static final SimpleClock SLOW_CLOCK = new SimpleClock(30);
  /** A 45 Hz clock instance */
  public static final SimpleClock MEDIUM_CLOCK = new SimpleClock(45);
  /** A 60 Hz clock instance */
  public static final SimpleClock FAST_CLOCK = new SimpleClock(60);

  private final double msPerTick;
  private long lastTick = -1;
  private long currentTick = -1;

  /**
   * Create a new instance oscillating with the provided hertz.
   * @param hertz the rate the timer oscillates on.
   */
  public SimpleClock(int hertz) {
    msPerTick = 1000.0 / hertz;
  }

  @Override
  public double deltaTime() {
    if (lastTick == -1 || currentTick == -1) {
      return 1;
    }
    return (currentTick - lastTick) / msPerTick;
  }

  @Override
  public void waitForNextTick() {
    lastTick = currentTick;
    currentTick = System.currentTimeMillis();
    long msToWait = lastTick == -1 || currentTick == -1 ? 0 : assumeNextTickDeltaMs();
    while(currentTick+msToWait > System.currentTimeMillis()){
      Thread.onSpinWait();
    }
  }

  private long assumeNextTickDeltaMs() {
    return Math.max(0, (long) ((2 * msPerTick) - (currentTick - lastTick)));
  }

  @Override
  public long getTime() {
    return currentTick;
  }
}
