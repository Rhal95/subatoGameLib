package name.panitz.game.framework.object;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.render.GraphicsTool;
import name.panitz.game.framework.render.Imageable;

public class ImageObject extends AbstractGameObject implements Imageable {
  /*+ Bilddateien werden zunächst als String des Dateinamens angegeben. */
  private String imagePath;

  public ImageObject(String imagePath) {
    super(0, 0);
    this.imagePath = imagePath;
  }

  public ImageObject(String imagePath, Vertex pos, Vertex motion) {
    super(0, 0, pos, motion);
    this.imagePath = imagePath;
  }

  /*+ Es folgen eine Reihe von Konstruktoren, die zu den Konstruktoren der Oberklasse passen. */

  public ImageObject(String imagePath, Vertex position) {
    super(0, 0, position);
    this.imagePath = imagePath;
  }

  public ImageObject(String imagePath, double width) {
    super(width);
    this.imagePath = imagePath;
  }

  @Override
  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  /*+ Und schließlich die Methode, mit der das Bild gezeichnet wird. */
  @Override
  public void paintTo(GraphicsTool g) {
    g.drawImage(getImagePath(), getPosition().x(), getPosition().y());
    Vertex dimensions = g.getImageSize(getImagePath());
    setHeight(dimensions.y());
    setWidth(dimensions.x());
  }
}

