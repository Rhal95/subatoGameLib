package name.panitz.game.framework.input;

public enum ButtonStatus {
  NONE, PRESSED, RELEASED
}
