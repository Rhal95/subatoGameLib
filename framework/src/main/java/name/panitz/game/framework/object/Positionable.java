package name.panitz.game.framework.object;

import name.panitz.game.framework.Vertex;

public interface Positionable {
  Vertex getPosition();
}
