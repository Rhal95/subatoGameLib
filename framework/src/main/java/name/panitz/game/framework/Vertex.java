package name.panitz.game.framework;

import java.io.Serializable;
import java.util.Objects;

/**
 * Two dimensional vector. Can be used to represent size, position, speed, acceleration, etc.
 * Objects of this class are immutable so all modifying operations return a new instance.
 * Can be represented as a record class in newer java versions (16+).
 */
public final class Vertex implements Serializable {
  /** Vector pointing up */
  public static final Vertex UP = new Vertex(0, 1);
  /** Vector pointing down */
  public static final Vertex DOWN = new Vertex(0, -1);
  /** Vector pointing right */
  public static final Vertex RIGHT = new Vertex(1, 0);
  /** Vector pointing left */
  public static final Vertex LEFT = new Vertex(-1, 0);
  /** Null vector */
  public static final Vertex ZERO = new Vertex(0, 0);

  private final double x;
  private final double y;

  /**
   * Constructs a new Vertex
   *
   * @param x horizontal value
   * @param y vertical value
   */
  public Vertex(double x, double y) {
    this.x = x;
    this.y = y;
  }

  /**
   * gets the x potion of the vertex.
   *
   * @return the x value of the vertex.
   */
  public double x() {
    return x;
  }

  /**
   * gets the y potion of the vector.
   *
   * @return the y value of the vector.
   */
  public double y() {
    return y;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }
    var that = (Vertex) obj;
    return Double.doubleToLongBits(this.x) == Double.doubleToLongBits(that.x) &&
               Double.doubleToLongBits(this.y) == Double.doubleToLongBits(that.y);
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }

  @Override
  public String toString() {
    return "(" + x + ", " + y + ")";
  }

  /**
   * Gets the length of this Vertex calculated using:
   * <p>
   * length = sqrt(x² + y²)
   *
   * @return the length of the vertex.
   */
  public double length() {
    return x == 0.0 && y == 0.0 ? 0.0 : Math.sqrt(x * x + y * y);
  }

  /**
   * Adds two Vertex by summing the x and y values respectively.
   *
   * @param that the vertex to add
   * @return a new vertex
   */
  public Vertex add(Vertex that) {
    return new Vertex(x + that.x, y + that.y);
  }

  /**
   * Scales the Vertex by the given multiplier.
   *
   * @param d the factor to scale this vertex by
   * @return a new vertex
   */
  public Vertex mult(double d) {
    return new Vertex(d * x, d * y);
  }

  /**
   * Multiplies two Vertex by multiplying the x and y values respectively.
   *
   * @param v the vertex to multiply with
   * @return a new vertex
   */
  public Vertex mult(Vertex v) {
    return new Vertex(v.x * x, v.y * y);
  }

  /**
   * Normalizes the vector by scaling it to length 1
   *
   * @return a new Vertex with length 1
   */
  public Vertex norm() {
    double length = length();
    return length > 0 ? mult(1 / length) : this;
  }
}

