package name.panitz.game.framework.object;

import name.panitz.game.framework.Vertex;

public class FallingImage extends ImageObject {
  /*+ Wir verwenden die Svchwerkraft als Konstante. */
  static final double G = 9.81;
  /* Sind wir am fallen/springen? */
  public boolean isJumping = false;
  /*+ In einem Feld wird die anfängliche Fallgeschwindigkeit vermerkt.*/
  double v0;
  /*+ Und ein Feld notiert, seit wieviel Ticks bereits gesprungen/gefallen wird. */
  int t = 0;

  /*+ Als Konstruktor wird der Konstruktor für Bildobjekte verwendet. */

  public FallingImage(String imageFileName, Vertex corner, Vertex movement) {
    super(imageFileName, corner, movement);
  }
  /*+ Wenn wir den Sprung stoppen wollen, so wird der zuletzt gemachte Weg rückgängig gemacht und die Bewegung auf 0 gesetzt. Wird landen. */

  public void stop() {
    moveBy(getVelocity().mult(-1.1));
    setVelocity(Vertex.ZERO);
    isJumping = false;
  }

  /*+ Wen ngestartet wird....*/

  public void restart() {
    double oldX = getVelocity().x();
    moveBy(getVelocity().mult(-1.1));
    setVelocity(new Vertex(-oldX, 0));
    isJumping = false;
  }

  /*+ Links/rechts Steuerung geht nur, wenn wir nicht springen. In der Luft lässt sich nicht die Richtung ändern. */

  public void left() {
    if (!isJumping) {
      setVelocity(getVelocity().mult(new Vertex(0, 1)).add(Vertex.LEFT));
    }
  }

  public void right() {
    if (!isJumping) {
      setVelocity(getVelocity().mult(new Vertex(0, 1)).add(Vertex.RIGHT));
    }
  }
  /*+ Wie starten einen Sprung mit der Anfangsgeschwindigkeit. */

  public void jump() {
    if (!isJumping) {
      startJump(-3.5);
    }
  }

  public void startJump(double v0) {
    isJumping = true;
    this.v0 = v0;
    t = 0;
  }
  /*+ Bewegung im Sprung verwendet die Schwerkraft, Anfangsgeschwindigkeit und Falllänge. Das ist Physik.*/

  @Override
  public void move() {
    if (isJumping) {
      t++;
      double v = v0 + G * t / 200;
      setVelocity(getVelocity().mult(Vertex.RIGHT).add(new Vertex(0, v)));
    }
    super.move();
  }
}
