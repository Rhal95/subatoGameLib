package name.panitz.game.framework;

import name.panitz.game.framework.clock.GameTimer;
import name.panitz.game.framework.input.GameInput;
import name.panitz.game.framework.logic.GameLogic;
import name.panitz.game.framework.logic.GameState;
import name.panitz.game.framework.logic.InputState;
import name.panitz.game.framework.render.GameRenderer;
import name.panitz.game.framework.sound.SoundTool;

/**
 * Core of a game binding all the required component together. All components are placed in the GameExecutor during
 * instantiation and then the playGame() method can be used to advance one game frame.
 * <p>
 * Usage:
 * <pre>{@code
 * GameLogic l; // this is your game logic implementation
 * GameInput i; // depending on the used backend
 * GameRenderer r; // depending on the used backend
 * GameClock t = SimpleClock.MEDIUM_CLOCK;
 * SoundTool s = SoundTool.NO_SOUND;
 * GameExecutor ex = new GameExecutor<>(l, i, r, t, s);
 * while(!ex.isStopped()) ex.playGame();
 * }</pre>
 *
 * @param <S> The GameState implementation of the game.
 * @param <I> The GameInput implementation of the game.
 */
/*+ Ein Spiel ist aufgeteilt in mehrere Komponenten die jeweils nur eine Aufgabe übernehmen.*/
public class GameExecutor<S extends GameState, I extends InputState<I>> {
  /*+ Die Spiellogik die das eigentliche Spiel beschreibt. */
  private final GameLogic<S, I> logic;
  private final GameInput<I> inputProvider;
  private final GameRenderer renderer;
  private final GameTimer timer;
  private final SoundTool speaker;

  /*+ Der InputState und GameState werden im Executor gehalten. */
  private I input;
  private S state;

  /**
   * Instantiate a new GameExecutor.
   *
   * @param logic         of the game to use
   * @param inputProvider of the used backend
   * @param renderer      of the used backend
   * @param timer         specifying how fast the game runs
   * @param speaker       specifying how sounds are played
   */
  public GameExecutor(GameLogic<S, I> logic, GameInput<I> inputProvider, GameRenderer renderer, GameTimer timer, SoundTool speaker) {
    this.logic = logic;
    this.inputProvider = inputProvider;
    this.renderer = renderer;
    this.timer = timer;
    this.speaker = speaker;

    /*+ Die Logik gibt an wie der Zustand am Anfang des Spiels ist. */
    this.state = logic.createInitialState();
    this.input = logic.createInitialInput();

    /*+ Die Logik gibt auch an in welcher Auflösung das Spiel laufen soll.*/
    this.renderer.setSize(logic.getSize());
  }

  /**
   * Runs one game Tick by doing the following procedure:
   * <ol>
   * <li> wait for the next tick according to the timer. </li>
   * <li> retrieve the passed time. </li>
   * <li> retrieve the inputs since last tick. </li>
   * <li> pass game state, inputs and deltaTicks to the logic to calculate the next game state.</li>
   * <li> play sounds. </li>
   * <li> render the game state. </li>
   * </ol>
   */
  /*+ Jetzt kann das Spiel gestartet werden.*/
  public void playGame() {
    timer.waitForNextTick();
    /*+ Der Timer sagt uns wie viele Spielticks abgearbeitet werden sollen. Das ist meistens eine Zahl ~1 */
    double deltaTicks = timer.deltaTime();

    input = inputProvider.getInput(this.input);
    /*+ Gebe die aktuellen Eingaben und Spielzustand an die Spiellogik um den nächsten Zustand zu berechnen. */
    state = logic.calculateTick(state, input, deltaTicks);
    /*+ Wenn Töne abgespielt werden sollen wird das hier gemacht. */
    speaker.playSounds(state.getAllSoundsToPlay());

    /*+ Benutze den Spielzustand um das Spiel zu rendern. */
    renderer.renderState(state);
  }

  /**
   * Tells if the game is still running.
   *
   * @return if the game has stopped.
   */
  public boolean isStopped() {
    return state.getStatus().isOver();
  }
}
