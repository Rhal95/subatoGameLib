package name.panitz.game.framework.logic;

import name.panitz.game.framework.object.Sizeable;

public interface GameLogic<S extends GameState, I extends InputState<I>> extends Sizeable {
  S calculateTick(S gameState, I input, double deltaTime);

  S createInitialState();

  I createInitialInput();
}
