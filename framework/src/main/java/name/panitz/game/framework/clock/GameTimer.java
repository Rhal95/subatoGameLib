package name.panitz.game.framework.clock;

/**
 * Interface abstracting time management for a game.
 */
public interface GameTimer {
  /**
   * The current system time.
   *
   * @return the time in ms
   */
  long getTime();

  /**
   * The time in ticks since the last {@code waitForNextTick}
   *
   * @return ticks since last wait
   */
  double deltaTime();

  /**
   * Sends the current thread to sleep for some time.
   * Tries to sleep until the next tick with no guarantee on precision.
   * That means after sleeping {@code deltaTime} can be smaller or greater than 1.
   */
  void waitForNextTick();
}
