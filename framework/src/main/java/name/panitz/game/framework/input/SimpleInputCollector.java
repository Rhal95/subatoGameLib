package name.panitz.game.framework.input;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;
import name.panitz.game.framework.logic.InputState;

public class SimpleInputCollector<I extends InputState<I>, K, M> implements GameInput<I> {
  private final List<Input> inputList = new LinkedList<>();
  private final Semaphore semaphore = new Semaphore(1);
  InputMapper<K, M> inputMapper;

  public SimpleInputCollector(InputMapper<K, M> inputMapper) {
    this.inputMapper = inputMapper;
  }

  public void addKeyPressed(K key) {
    semaphore.acquireUninterruptibly();
    inputList.add(new KeyInput(inputMapper.toKeyCode(key), ButtonStatus.PRESSED));
    semaphore.release();
  }

  public void addKeyReleased(K key) {
    semaphore.acquireUninterruptibly();
    inputList.add(new KeyInput(inputMapper.toKeyCode(key), ButtonStatus.RELEASED));
    semaphore.release();
  }

  public void addMouseMoved(M mouse) {
    semaphore.acquireUninterruptibly();
    inputList.add(new MouseInput(inputMapper.toMousePosition(mouse), inputMapper.toButtonCode(mouse), ButtonStatus.NONE));
    semaphore.release();
  }

  public void addMousePressed(M mouse) {
    semaphore.acquireUninterruptibly();
    inputList.add(new MouseInput(inputMapper.toMousePosition(mouse), inputMapper.toButtonCode(mouse), ButtonStatus.PRESSED));
    semaphore.release();
  }

  public void addMouseReleased(M mouse) {
    semaphore.acquireUninterruptibly();
    inputList.add(new MouseInput(inputMapper.toMousePosition(mouse), inputMapper.toButtonCode(mouse), ButtonStatus.RELEASED));
    semaphore.release();
  }

  @Override
  public I getInput(I inputstate) {
    var accumulator = new Object() {
      I obj = inputstate;
    };
    semaphore.acquireUninterruptibly();
    var inputs = new LinkedList<>(inputList);

    inputs.forEach(input -> accumulator.obj = accumulator.obj.handleInput(input));

    inputList.removeAll(inputs);
    semaphore.release();
    return accumulator.obj;
  }
}
