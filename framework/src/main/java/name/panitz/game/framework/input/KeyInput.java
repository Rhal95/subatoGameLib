package name.panitz.game.framework.input;

import java.util.Objects;

public final class KeyInput implements Input {
  private final KeyCode keyCode;
  private final ButtonStatus status;

  KeyInput(KeyCode keyCode,
           ButtonStatus status) {
    this.keyCode = keyCode;
    this.status = status;
  }

  public KeyCode keyCode() {
    return keyCode;
  }

  public ButtonStatus status() {
    return status;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) return true;
    if (obj == null || obj.getClass() != this.getClass()) return false;
    var that = (KeyInput) obj;
    return Objects.equals(this.keyCode, that.keyCode) &&
               Objects.equals(this.status, that.status);
  }

  @Override
  public int hashCode() {
    return Objects.hash(keyCode, status);
  }

  public static KeyInput of(KeyCode keyCode, ButtonStatus status) {
    return new KeyInput(keyCode, status);
  }

  @Override
  public String toString() {
    return "KeyInput[" +
               "keyCode=" + keyCode + ", " +
               "status=" + status + ']';
  }
}
