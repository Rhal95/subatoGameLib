package name.panitz.game.framework;

import name.panitz.game.DummyInputState;
import name.panitz.game.DummyState;
import name.panitz.game.framework.clock.GameTimer;
import name.panitz.game.framework.input.GameInput;
import name.panitz.game.framework.logic.GameLogic;
import name.panitz.game.framework.render.GameRenderer;
import name.panitz.game.framework.sound.SoundTool;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

class GameExecutorTest {
  private final GameLogic<DummyState, DummyInputState> logic = Mockito.mock(GameLogic.class);
  private final GameInput<DummyInputState> input = Mockito.mock(GameInput.class);
  private final GameRenderer renderer = Mockito.mock(GameRenderer.class);
  private final GameTimer timer = Mockito.mock(GameTimer.class);
  private final SoundTool speaker = Mockito.mock(SoundTool.class);

  private DummyState gameState;
  private DummyInputState gameInput;

  private GameExecutor<DummyState, DummyInputState> sut;

  @BeforeEach
  void setUp() {
    gameState = new DummyState();
    gameInput = new DummyInputState();

    when(logic.createInitialState()).thenReturn(gameState);
    when(logic.createInitialInput()).thenReturn(gameInput);
    sut = new GameExecutor<>(logic, input, renderer, timer, speaker);

    verify(logic).createInitialInput();
    verify(logic).createInitialState();
    verify(logic).getSize();
    verify(renderer).setSize(any());
    verifyNoMoreInteractions(logic, input, renderer, timer, speaker);
  }

  @Test
  void runOneGametick() {
    when(timer.deltaTime()).thenReturn(1.0);
    when(logic.calculateTick(any(), any(), anyDouble())).thenReturn(gameState);

    sut.playGame();

    verify(timer).waitForNextTick();
    verify(input).getInput(any());
    verify(timer).deltaTime();
    verify(speaker).playSounds(any());
    verify(logic, times(1)).calculateTick(any(), any(), anyDouble());
    verify(renderer, times(1)).renderState(any());
  }

  @Test
  void callIsStopped() {
    Assertions.assertFalse(sut.isStopped());
    gameState.gameStatus = GameStatus.PAUSED;
    Assertions.assertFalse(sut.isStopped());
    gameState.gameStatus = GameStatus.RUNNING;
    Assertions.assertFalse(sut.isStopped());
    gameState.gameStatus = GameStatus.LOST;
    Assertions.assertTrue(sut.isStopped());
    gameState.gameStatus = GameStatus.WON;
    Assertions.assertTrue(sut.isStopped());
  }

  @AfterEach
  void tearDown() {
    verifyNoMoreInteractions(logic, input, renderer, timer, speaker);
  }
}
