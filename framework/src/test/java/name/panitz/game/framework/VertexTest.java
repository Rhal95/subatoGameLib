package name.panitz.game.framework;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class VertexTest {

  private void assertVertexEqual(Vertex expected, Vertex actual, double epsilon, String message) {
    assertAll(message, () -> {
      assertEquals(expected.x(), actual.x(), epsilon, "x component is wrong");
      assertEquals(expected.y(), actual.y(), epsilon, "y component is wrong");
    });
  }

  private void assertVertexEqual(Vertex expected, Vertex actual, String message) {
    assertVertexEqual(expected, actual, 0.0, message);
  }

  @Test
  void length() {
    assertEquals(0.0, Vertex.ZERO.length(), 0.0, "length of zero vector wrong");
    assertEquals(1.0, Vertex.DOWN.length(), 0.0, "length of down vector wrong");
    assertEquals(1.0, Vertex.UP.length(), 0.0, "length of up vector wrong");
    assertEquals(1.0, Vertex.RIGHT.length(), 0.0, "length of right vector wrong");
    assertEquals(1.0, Vertex.LEFT.length(), 0.0, "length of left vector wrong");
    assertEquals(5.0, new Vertex(3, 4).length(), 0.0, "length of (3,4) vector wrong");
    assertEquals(1000.0, new Vertex(800, 600).length(), 0.0, "length of (800, 600) vector wrong");
    assertEquals(99.528, new Vertex(12.34, 98.76).length(), 0.001, "length of (12.34, 98.76) vector wrong");
  }

  @Test
  void add() {
    assertVertexEqual(Vertex.ZERO, Vertex.ZERO.add(Vertex.ZERO), "(0,0)+(0,0)");
    assertVertexEqual(Vertex.UP, Vertex.UP.add(Vertex.ZERO), "(0,1)+(0,0)");
    assertVertexEqual(Vertex.UP, Vertex.ZERO.add(Vertex.UP), "(0,0)+(0,1)");
    assertVertexEqual(new Vertex(-50, 25), new Vertex(50, 100).add(new Vertex(-100, -75)), "(50,100)+(-100,-75)");
  }

  @Test
  void mult() {
    assertVertexEqual(Vertex.ZERO, Vertex.ZERO.mult(Vertex.ZERO), "(0,0)*(0,0)");
    assertVertexEqual(Vertex.UP, Vertex.UP.mult(Vertex.UP), "(0,1)*(0,1)");
    assertVertexEqual(Vertex.DOWN, Vertex.UP.mult(Vertex.DOWN), "(0,1)*(0,-1)");
    assertVertexEqual(new Vertex(-50, 25), new Vertex(50, 100).mult(new Vertex(-1, 0.25)), "(50,100)*(-1,0.25)");
  }

  @Test
  void testMult() {
    assertVertexEqual(Vertex.ZERO, Vertex.UP.mult(0), "(0,0)*0");
    assertVertexEqual(Vertex.ZERO, new Vertex(123, -987).mult(0), "(123,-987)*0");
    assertVertexEqual(new Vertex(-50, 25), new Vertex(25, -12.5).mult(-2), "(25,-12.5)*-2");
  }

  @Test
  void norm() {
    assertVertexEqual(Vertex.RIGHT, new Vertex(12345, 0).norm(), "(12345, 0)");
    assertVertexEqual(new Vertex(0.7071, 0.7071), new Vertex(123.45, 123.45).norm(), 0.0001, "(123.45, 123.45)");
  }
}
