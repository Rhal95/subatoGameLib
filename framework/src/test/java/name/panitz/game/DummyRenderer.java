package name.panitz.game;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.logic.GameState;
import name.panitz.game.framework.render.GameRenderer;

/**
 * Dummy implementation for testing the framework
 */
public class DummyRenderer implements GameRenderer {
  /**
   * Public, so it can be retrieved in a test.
   */
  public GameState state;

  @Override
  public void renderState(GameState state) {
    this.state = state;
  }

  @Override
  public void setSize(Vertex size) {

  }
}
