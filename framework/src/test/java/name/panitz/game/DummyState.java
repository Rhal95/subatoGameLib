package name.panitz.game;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import name.panitz.game.framework.GameStatus;
import name.panitz.game.framework.logic.GameState;
import name.panitz.game.framework.object.GameObject;
import name.panitz.game.framework.render.Paintable;
import name.panitz.game.framework.sound.Soundable;

/**
 * Dummy implementation for testing the framework
 */
public class DummyState implements GameState {
  /**
   * Public, so it can be retrieved in a test.
   */
  public List<GameObject> gameObjects = new LinkedList<>();
  /**
   * Public, so it can be retrieved in a test.
   */
  public List<Soundable> soundObjects = new LinkedList<>();
  /**
   * Public, so it can be retrieved in a test.
   */
  public GameStatus gameStatus = GameStatus.NOT_STARTED;

  @Override
  public GameStatus getStatus() {
    return gameStatus;
  }

  @Override
  public List<GameObject> getAllGameObjects() {
    return gameObjects;
  }

  @Override
  public List<Paintable> getAllObjectsToPaint() {
    return Collections.unmodifiableList(getAllGameObjects());
  }

  @Override
  public List<Soundable> getAllSoundsToPlay() {
    return soundObjects;
  }
}
