package name.panitz.game;

import java.util.LinkedList;
import java.util.List;
import name.panitz.game.framework.input.Input;
import name.panitz.game.framework.logic.InputState;

/**
 * Dummy implementation for testing the framework
 */
public class DummyInputState implements InputState<DummyInputState> {
  /**
   * Public, so it can be retrieved in a test.
   */
  public List<Input> inputs = new LinkedList<>();

  @Override
  public DummyInputState handleInput(Input input) {
    inputs.add(input);
    return this;
  }
}
