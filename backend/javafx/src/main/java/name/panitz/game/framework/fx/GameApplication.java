package name.panitz.game.framework.fx;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.input.GameInput;
import name.panitz.game.framework.input.SimpleInputCollector;
import name.panitz.game.framework.logic.GameState;
import name.panitz.game.framework.logic.InputState;
import name.panitz.game.framework.render.GameRenderer;
import name.panitz.game.framework.render.GraphicsTool;


public class GameApplication<I extends InputState<I>> extends Application implements GameRenderer, GameInput<I> {
  Canvas canvas;
  GraphicsTool graphicsTool;
  SimpleInputCollector<I, KeyEvent, MouseEvent> inputCollector = new SimpleInputCollector<>(FxInputMapper.MAPPER);

  private GameApplication() {
    canvas = new Canvas();
    graphicsTool = new FXContextTool(canvas.getGraphicsContext2D());
  }

  public static <M extends InputState<M>> GameApplication<M> startFxGame() {
    var application = new GameApplication<M>();
    Platform.startup(() -> application.start(new Stage()));
    Platform.setImplicitExit(true);
    return application;
  }

  public void start(Stage stage) {

    BorderPane bp = new BorderPane();
    bp.setCenter(canvas);

    stage.addEventHandler(KeyEvent.KEY_PRESSED, inputCollector::addKeyPressed);
    stage.addEventHandler(KeyEvent.KEY_RELEASED, inputCollector::addKeyReleased);
    stage.addEventHandler(KeyEvent.KEY_TYPED, key -> {
      inputCollector.addKeyPressed(key);
      inputCollector.addKeyReleased(key);
    });
    canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, inputCollector::addMousePressed);
    canvas.addEventHandler(MouseEvent.MOUSE_RELEASED, inputCollector::addMouseReleased);
    canvas.addEventHandler(MouseEvent.MOUSE_MOVED, inputCollector::addMouseMoved);
    canvas.addEventHandler(MouseEvent.MOUSE_CLICKED, mouse -> {
      inputCollector.addMousePressed(mouse);
      inputCollector.addMouseReleased(mouse);
    });
    canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, inputCollector::addMousePressed);

    FlowPane fp = new FlowPane();
    bp.setBottom(fp);

    stage.setScene(new Scene(bp));
    stage.show();
  }

  @Override
  public I getInput(I input) {
    return inputCollector.getInput(input);
  }

  @Override
  public void renderState(GameState state) {
    canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    state.getAllObjectsToPaint().forEach(gameObject -> gameObject.paintTo(graphicsTool));
  }

  @Override
  public void setSize(Vertex size) {
    canvas.setWidth(size.x());
    canvas.setHeight(size.y());
  }
}

