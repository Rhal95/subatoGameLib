package name.panitz.game.framework.fx;

import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.input.ButtonCode;
import name.panitz.game.framework.input.InputMapper;
import name.panitz.game.framework.input.KeyCode;

public class FxInputMapper implements InputMapper<KeyEvent, MouseEvent> {
  public static final FxInputMapper MAPPER = new FxInputMapper();

  private FxInputMapper() {
  }

  @Override
  public KeyCode toKeyCode(KeyEvent key) {
    return switch (key.getCode()) {
      case SPACE -> KeyCode.VK_SPACE;
      case LEFT, KP_LEFT -> KeyCode.LEFT_ARROW;
      case UP, KP_UP -> KeyCode.UP_ARROW;
      case RIGHT, KP_RIGHT -> KeyCode.RIGHT_ARROW;
      case DOWN, KP_DOWN -> KeyCode.DOWN_ARROW;
      case A -> KeyCode.VK_A;
      case B -> KeyCode.VK_B;
      case C -> KeyCode.VK_C;
      case D -> KeyCode.VK_D;
      case E -> KeyCode.VK_E;
      case F -> KeyCode.VK_F;
      case G -> KeyCode.VK_G;
      case H -> KeyCode.VK_H;
      case I -> KeyCode.VK_I;
      case J -> KeyCode.VK_J;
      case K -> KeyCode.VK_K;
      case L -> KeyCode.VK_L;
      case M -> KeyCode.VK_M;
      case N -> KeyCode.VK_N;
      case O -> KeyCode.VK_O;
      case P -> KeyCode.VK_P;
      case Q -> KeyCode.VK_Q;
      case R -> KeyCode.VK_R;
      case S -> KeyCode.VK_S;
      case T -> KeyCode.VK_T;
      case U -> KeyCode.VK_U;
      case V -> KeyCode.VK_V;
      case W -> KeyCode.VK_W;
      case X -> KeyCode.VK_X;
      case Y -> KeyCode.VK_Y;
      case Z -> KeyCode.VK_Z;
      default -> null;
    };
  }

  @Override
  public ButtonCode toButtonCode(MouseEvent mouse) {
    return switch (mouse.getButton()) {
      case PRIMARY -> ButtonCode.LEFT_MOUSE;
      case MIDDLE -> ButtonCode.MIDDLE_MOUSE;
      case SECONDARY -> ButtonCode.RIGHT_MOUSE;
      default -> ButtonCode.NONE;
    };
  }

  @Override
  public Vertex toMousePosition(MouseEvent mouse) {
    return new Vertex(mouse.getX(), mouse.getY());
  }
}
