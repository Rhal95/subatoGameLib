package name.panitz.game.framework.fx;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.render.AbstractGraphicsTool;

public class FXContextTool extends AbstractGraphicsTool<Image> {
  GraphicsContext gc;

  public FXContextTool(GraphicsContext gc) {
    this.gc = gc;
  }

  @Override
  public void drawImage(String img, double x, double y) {
    getImage(img).ifPresent(i -> gc.drawImage(i, x, y));
  }

  @Override
  public void drawRect(double x, double y, double w, double h) {
    gc.setLineWidth(1);
    gc.strokeRect(x, y, w, h);
  }

  @Override
  public void fillRect(double x, double y, double w, double h) {
    gc.fillRect(x, y, w, h);
  }

  @Override
  public void drawOval(double x, double y, double w, double h) {
    gc.setLineWidth(2);
    gc.strokeOval(x, y, w, h);
  }

  @Override
  public void fillOval(double x, double y, double w, double h) {
    gc.fillOval(x, y, w, h);
  }

  @Override
  public void drawLine(double x1, double y1, double x2, double y2) {
    gc.strokeLine(x1, y1, x2, y2);
  }

  @Override
  public void drawString(double x, double y, int fontSize, String fontName, String text) {
    gc.setFont(new Font(fontName, fontSize));
    gc.fillText(text, x, y);
  }

  @Override
  protected Vertex getSizes(Image image) {
    return new Vertex(image.getWidth(), image.getHeight());
  }

  @Override
  protected Image loadImage(String name) {
    return new Image(getClass().getClassLoader().getResourceAsStream(name));
  }

  @Override
  public void setColor(double r, double g, double b) {
    gc.setFill(new javafx.scene.paint.Color(r, g, b, 1));
    gc.setStroke(new javafx.scene.paint.Color(r, g, b, 1));
  }
}
