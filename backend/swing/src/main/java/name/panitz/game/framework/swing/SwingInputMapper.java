package name.panitz.game.framework.swing;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.input.ButtonCode;
import name.panitz.game.framework.input.InputMapper;
import name.panitz.game.framework.input.KeyCode;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class SwingInputMapper implements InputMapper<KeyEvent, MouseEvent> {
  static SwingInputMapper MAPPER = new SwingInputMapper();

  @Override
  public KeyCode toKeyCode(KeyEvent keyCode) {
    return switch (keyCode.getKeyCode()) {
      case KeyEvent.VK_A:
        yield KeyCode.VK_A;
      case KeyEvent.VK_B:
        yield KeyCode.VK_B;
      case KeyEvent.VK_C:
        yield KeyCode.VK_C;
      case KeyEvent.VK_D:
        yield KeyCode.VK_D;
      case KeyEvent.VK_E:
        yield KeyCode.VK_E;
      case KeyEvent.VK_F:
        yield KeyCode.VK_F;
      case KeyEvent.VK_G:
        yield KeyCode.VK_G;
      case KeyEvent.VK_H:
        yield KeyCode.VK_H;
      case KeyEvent.VK_I:
        yield KeyCode.VK_I;
      case KeyEvent.VK_J:
        yield KeyCode.VK_J;
      case KeyEvent.VK_K:
        yield KeyCode.VK_K;
      case KeyEvent.VK_L:
        yield KeyCode.VK_L;
      case KeyEvent.VK_M:
        yield KeyCode.VK_M;
      case KeyEvent.VK_N:
        yield KeyCode.VK_N;
      case KeyEvent.VK_O:
        yield KeyCode.VK_O;
      case KeyEvent.VK_P:
        yield KeyCode.VK_P;
      case KeyEvent.VK_Q:
        yield KeyCode.VK_Q;
      case KeyEvent.VK_R:
        yield KeyCode.VK_R;
      case KeyEvent.VK_S:
        yield KeyCode.VK_S;
      case KeyEvent.VK_T:
        yield KeyCode.VK_T;
      case KeyEvent.VK_U:
        yield KeyCode.VK_U;
      case KeyEvent.VK_V:
        yield KeyCode.VK_V;
      case KeyEvent.VK_W:
        yield KeyCode.VK_W;
      case KeyEvent.VK_X:
        yield KeyCode.VK_X;
      case KeyEvent.VK_Y:
        yield KeyCode.VK_Y;
      case KeyEvent.VK_Z:
        yield KeyCode.VK_Z;
      case KeyEvent.VK_SPACE:
        yield KeyCode.VK_SPACE;
      case KeyEvent.VK_UP:
        yield KeyCode.UP_ARROW;
      case KeyEvent.VK_DOWN:
        yield KeyCode.DOWN_ARROW;
      case KeyEvent.VK_LEFT:
        yield KeyCode.LEFT_ARROW;
      case KeyEvent.VK_RIGHT:
        yield KeyCode.RIGHT_ARROW;
      default:
        yield null;
    };
  }

  @Override
  public ButtonCode toButtonCode(MouseEvent mouseButton) {
    return switch (mouseButton.getButton()) {
      case MouseEvent.BUTTON1 -> ButtonCode.LEFT_MOUSE;
      case MouseEvent.BUTTON2 -> ButtonCode.RIGHT_MOUSE;
      case MouseEvent.BUTTON3 -> ButtonCode.MIDDLE_MOUSE;
      default -> ButtonCode.NONE;
    };
  }

  @Override
  public Vertex toMousePosition(MouseEvent mouse) {
    return new Vertex(mouse.getX(), mouse.getY());
  }
}
