package name.panitz.game.framework.swing;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;
import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.input.GameInput;
import name.panitz.game.framework.input.SimpleInputCollector;
import name.panitz.game.framework.logic.GameState;
import name.panitz.game.framework.logic.InputState;
import name.panitz.game.framework.render.GameRenderer;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class SwingGame<I extends InputState<I>> extends JPanel implements GameRenderer, GameInput<I> {
  private final transient SimpleInputCollector<I, KeyEvent, MouseEvent> inputCollector = new SimpleInputCollector<>(SwingInputMapper.MAPPER);
  private final JFrame frame = new JFrame();
  private GameState state = null;

  public SwingGame() {
    frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    frame.add(this);
    frame.pack();
    frame.setVisible(true);
    frame.repaint();

    frame.addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        inputCollector.addKeyPressed(e);
      }

      @Override
      public void keyReleased(KeyEvent e) {
        inputCollector.addKeyReleased(e);
      }

      @Override
      public void keyTyped(KeyEvent e) {
        inputCollector.addKeyPressed(e);
        inputCollector.addKeyReleased(e);
      }
    });
    this.addMouseListener(new MouseInputAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {
        inputCollector.addMousePressed(e);
      }

      @Override
      public void mouseReleased(MouseEvent e) {
        inputCollector.addMouseReleased(e);
      }
    });
    this.addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        inputCollector.addMouseMoved(e);
      }

      @Override
      public void mouseDragged(MouseEvent e) {
        inputCollector.addMousePressed(e);
      }
    });
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    if (state == null) return;
    state.getAllObjectsToPaint().forEach(o -> o.paintTo(new SwingGraphicsTool(g)));
  }

  @Override
  public void renderState(GameState state) {
    this.state = state;
    frame.pack();
    frame.repaint();
  }

  @Override
  public I getInput(I inputState) {
    return inputCollector.getInput(inputState);
  }

  @Override
  public void setSize(Vertex size) {
    var dimension = new Dimension((int) size.x(), (int) size.y());
    this.setSize(dimension);
    this.setPreferredSize(dimension);
  }
}
