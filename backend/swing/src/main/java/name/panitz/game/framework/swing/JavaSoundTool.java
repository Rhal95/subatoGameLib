package name.panitz.game.framework.swing;

import name.panitz.game.framework.sound.SoundTool;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class JavaSoundTool implements SoundTool {

  Map<String, AudioInputStream> audioInputStreamMap = new HashMap<>();

  @Override
  public AudioInputStream loadSound(String fileName) {
    try {
      InputStream src = getClass().getClassLoader().getResourceAsStream(fileName);
      InputStream bufferedIn = new BufferedInputStream(src);
      // File laden

      return AudioSystem.getAudioInputStream(bufferedIn);
    } catch (UnsupportedAudioFileException | IOException e) {
      e.printStackTrace();
    }
    System.out.println("ups?");
    return null;
  }

  private AudioInputStream getSound(String name) {
    return Optional.ofNullable(audioInputStreamMap.get(name))
               .orElseGet(() -> {
                 var sound = loadSound(name);
                 audioInputStreamMap.put(name, sound);
                 return sound;
               });
  }

  @Override
  public void playSound(String sound) {
    Clip clip;
    try {
      clip = AudioSystem.getClip();
      AudioInputStream audioInputStream = getSound(sound);
      audioInputStream.mark(10_000_000);
      clip.open(audioInputStream);
      clip.start();
      try {
        audioInputStream.reset();
      } catch (IOException e) {
        System.out.println("Sound " + sound + " invalid, reloading next time.");
        audioInputStreamMap.remove(sound);
      }
    } catch (LineUnavailableException | IOException e) {
      e.printStackTrace();
    }
  }

}
