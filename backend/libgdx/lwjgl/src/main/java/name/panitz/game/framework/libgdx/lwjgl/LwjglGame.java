package name.panitz.game.framework.libgdx.lwjgl;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import name.panitz.game.framework.libgdx.GdxGame;
import name.panitz.game.framework.logic.InputState;

public class LwjglGame<I extends InputState<I>> extends GdxGame<I> {

  public LwjglGame() {
    var config = new LwjglApplicationConfiguration();
    config.vSyncEnabled = false;
    config.foregroundFPS = Integer.MAX_VALUE;
    new LwjglApplication(this, config);
  }
}
