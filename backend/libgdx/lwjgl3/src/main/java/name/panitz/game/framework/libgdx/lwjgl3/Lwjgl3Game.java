package name.panitz.game.framework.libgdx.lwjgl3;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import name.panitz.game.framework.libgdx.GdxGame;

public class Lwjgl3Game extends GdxGame {

  public Lwjgl3Game() {
    var config = new Lwjgl3ApplicationConfiguration();
    config.setForegroundFPS(Integer.MAX_VALUE);
    config.useVsync(false);
    new Lwjgl3Application(this, config);
  }
}
