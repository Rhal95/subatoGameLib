package name.panitz.game.framework.libgdx;

import name.panitz.game.framework.Vertex;

public record GdxMouse(Vertex position, int button) {
}
