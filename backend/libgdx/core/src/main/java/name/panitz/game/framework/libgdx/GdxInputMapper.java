package name.panitz.game.framework.libgdx;

import com.badlogic.gdx.Input;
import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.input.ButtonCode;
import name.panitz.game.framework.input.InputMapper;
import name.panitz.game.framework.input.KeyCode;

public class GdxInputMapper implements InputMapper<Integer, GdxMouse> {

  public static final GdxInputMapper MAPPER = new GdxInputMapper();

  private GdxInputMapper() {
  }

  public KeyCode toKeyCode(Integer keyCode) {
    return switch (keyCode) {
      case Input.Keys.A:
        yield KeyCode.VK_A;
      case Input.Keys.B:
        yield KeyCode.VK_B;
      case Input.Keys.C:
        yield KeyCode.VK_C;
      case Input.Keys.D:
        yield KeyCode.VK_D;
      case Input.Keys.E:
        yield KeyCode.VK_E;
      case Input.Keys.F:
        yield KeyCode.VK_F;
      case Input.Keys.G:
        yield KeyCode.VK_G;
      case Input.Keys.H:
        yield KeyCode.VK_H;
      case Input.Keys.I:
        yield KeyCode.VK_I;
      case Input.Keys.J:
        yield KeyCode.VK_J;
      case Input.Keys.K:
        yield KeyCode.VK_K;
      case Input.Keys.L:
        yield KeyCode.VK_L;
      case Input.Keys.M:
        yield KeyCode.VK_M;
      case Input.Keys.N:
        yield KeyCode.VK_N;
      case Input.Keys.O:
        yield KeyCode.VK_O;
      case Input.Keys.P:
        yield KeyCode.VK_P;
      case Input.Keys.Q:
        yield KeyCode.VK_Q;
      case Input.Keys.R:
        yield KeyCode.VK_R;
      case Input.Keys.S:
        yield KeyCode.VK_S;
      case Input.Keys.T:
        yield KeyCode.VK_T;
      case Input.Keys.U:
        yield KeyCode.VK_U;
      case Input.Keys.V:
        yield KeyCode.VK_V;
      case Input.Keys.W:
        yield KeyCode.VK_W;
      case Input.Keys.X:
        yield KeyCode.VK_X;
      case Input.Keys.Y:
        yield KeyCode.VK_Y;
      case Input.Keys.Z:
        yield KeyCode.VK_Z;
      case Input.Keys.SPACE:
        yield KeyCode.VK_SPACE;
      case Input.Keys.UP:
        yield KeyCode.UP_ARROW;
      case Input.Keys.DOWN:
        yield KeyCode.DOWN_ARROW;
      case Input.Keys.LEFT:
        yield KeyCode.LEFT_ARROW;
      case Input.Keys.RIGHT:
        yield KeyCode.RIGHT_ARROW;
      default:
        yield null;
    };
  }

  @Override
  public ButtonCode toButtonCode(GdxMouse mouse) {
    return switch (mouse.button()) {
      case Input.Buttons.LEFT -> ButtonCode.LEFT_MOUSE;
      case Input.Buttons.RIGHT -> ButtonCode.RIGHT_MOUSE;
      case Input.Buttons.MIDDLE -> ButtonCode.MIDDLE_MOUSE;
      default -> ButtonCode.NONE;
    };
  }

  @Override
  public Vertex toMousePosition(GdxMouse mouse) {
    return mouse.position();
  }
}
