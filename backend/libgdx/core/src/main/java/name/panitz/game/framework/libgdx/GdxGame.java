package name.panitz.game.framework.libgdx;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import static com.badlogic.gdx.graphics.GL20.GL_COLOR_BUFFER_BIT;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.input.GameInput;
import name.panitz.game.framework.input.SimpleInputCollector;
import name.panitz.game.framework.logic.GameState;
import name.panitz.game.framework.logic.InputState;
import name.panitz.game.framework.render.GameRenderer;

public class GdxGame<I extends InputState<I>> extends ApplicationAdapter implements GameRenderer, GameInput<I> {
  SpriteBatch batch;
  ShapeRenderer shapeRenderer;

  GdxGraphicsTool graphicsTool;

  GameState lastState = null;
  SimpleInputCollector<I, Integer, GdxMouse> inputCollector = new SimpleInputCollector<>(GdxInputMapper.MAPPER);

  @Override
  public void create() {
    super.create();
    batch = new SpriteBatch();
    shapeRenderer = new ShapeRenderer();
    graphicsTool = new GdxGraphicsTool(batch, shapeRenderer);
    Gdx.app.getInput().setInputProcessor(new InputAdapter() {
      @Override
      public boolean keyDown(int keycode) {
        inputCollector.addKeyPressed(keycode);
        return true;
      }

      @Override
      public boolean keyUp(int keycode) {
        inputCollector.addKeyReleased(keycode);
        return true;
      }

      @Override
      public boolean mouseMoved(int screenX, int screenY) {
        inputCollector.addMouseMoved(new GdxMouse(new Vertex(screenX, screenY), -1));
        return true;
      }

      @Override
      public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        inputCollector.addMousePressed(new GdxMouse(new Vertex(screenX, screenY), button));
        return true;
      }

      @Override
      public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        inputCollector.addMouseReleased(new GdxMouse(new Vertex(screenX, screenY), button));
        return true;
      }

      @Override
      public boolean touchDragged(int screenX, int screenY, int pointer) {
        inputCollector.addMouseMoved(new GdxMouse(new Vertex(screenX, screenY), Input.Buttons.LEFT));
        return true;
      }

    });

  }

  @Override
  public void render() {
    if (batch == null || shapeRenderer == null) return;
    Gdx.gl.glClearColor(1, 1, 1, 1);
    Gdx.gl.glClear(GL_COLOR_BUFFER_BIT);
    batch.begin();
    shapeRenderer.setAutoShapeType(true);
    shapeRenderer.begin();

    if (lastState != null) {
      lastState.getAllObjectsToPaint().forEach(gameObject -> gameObject.paintTo(graphicsTool));
    }
    new BitmapFont().draw(batch, Gdx.graphics.getFramesPerSecond() + "", 0, 20);
    batch.end();
    shapeRenderer.end();
  }

  @Override
  public void dispose() {
    super.dispose();
    batch.dispose();
    shapeRenderer.dispose();
    // the graphicsTool can not be used after disposing the renderers so we set it to null here
    graphicsTool = null;
  }

  @Override
  public I getInput(I input) {
    return inputCollector.getInput(input);
  }

  @Override
  public void renderState(GameState state) {
    this.lastState = state;
  }

  @Override
  public void setSize(Vertex size) {
    Gdx.graphics.setWindowedMode((int) size.x(), (int) size.y());
  }
}
