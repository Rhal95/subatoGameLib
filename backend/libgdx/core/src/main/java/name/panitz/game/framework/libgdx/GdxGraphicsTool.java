package name.panitz.game.framework.libgdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.render.AbstractGraphicsTool;

public class GdxGraphicsTool extends AbstractGraphicsTool<Texture> {
  private final SpriteBatch spriteBatch;
  private final ShapeRenderer shapeRenderer;

  public GdxGraphicsTool(SpriteBatch spriteBatch, ShapeRenderer shapeRenderer) {
    this.spriteBatch = spriteBatch;
    this.shapeRenderer = shapeRenderer;
  }

  private int height() {
    return Gdx.graphics.getHeight();
  }

  @Override
  public void drawImage(String img, double x, double y) {
    getImage(img).ifPresent(i -> spriteBatch.draw(i, (int) x, (int) (height() - y - i.getHeight())));
  }

  @Override
  public void drawRect(double x, double y, double w, double h) {
    shapeRenderer.set(ShapeRenderer.ShapeType.Line);
    shapeRenderer.rect((float) x, (float) (height() - (y + h)), (float) w, (float) h);
  }

  @Override
  public void fillRect(double x, double y, double w, double h) {
    shapeRenderer.set(ShapeRenderer.ShapeType.Filled);
    shapeRenderer.rect((float) x, (float) (height() - (y + h)), (float) w, (float) h);
  }

  @Override
  public void drawOval(double x, double y, double w, double h) {
    shapeRenderer.set(ShapeRenderer.ShapeType.Line);
    shapeRenderer.ellipse((float) x, (float) (height() - (y + h)), (float) w, (float) h);
  }

  @Override
  public void fillOval(double x, double y, double w, double h) {
    shapeRenderer.set(ShapeRenderer.ShapeType.Filled);
    shapeRenderer.ellipse((float) x, (float) (height() - (y + h)), (float) w, (float) h);
  }

  @Override
  public void drawLine(double x1, double y1, double x2, double y2) {
    shapeRenderer.line((float) x1, (float) (height() - y1), (float) x2, (float) (height() - y2));
  }

  @Override
  public void setColor(double red, double green, double blue) {
    shapeRenderer.setColor((float) red, (float) green, (float) blue, 1);
  }

  @Override
  public void drawString(double x, double y, int fntsize, String fntName, String text) {
    var font = new BitmapFont(false);
    font.draw(spriteBatch, text, (float) x, (float) (height() - (y)));
  }

  @Override
  protected Vertex getSizes(Texture image) {
    return new Vertex(image.getWidth(), image.getHeight());
  }

  @Override
  protected Texture loadImage(String name) {
    return new Texture(name);
  }
}
