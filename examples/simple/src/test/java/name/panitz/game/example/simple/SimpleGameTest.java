package name.panitz.game.example.simple;

import java.util.List;
import java.util.stream.Collectors;
import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.object.GameObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SimpleGameTest {
  private SimpleGameState gameState;
  private SimpleGameInputState gameInput;
  private SimpleGame simpleGame;

  @BeforeEach
  void setUp() {
    simpleGame = new SimpleGame();
    gameState = simpleGame.createInitialState();
    gameInput = simpleGame.createInitialInput();
  }

  @Test
  void gameGetsInitialized() {
    assertEquals(600, simpleGame.getHeight());
    assertEquals(800, simpleGame.getWidth());
    assertEquals(9, gameState.getAllGameObjects().size());

    assertEquals("Kleines Beispielspiel", gameState.sticheText().text);

    assertEquals("wiese.jpg", gameState.hintergrund().getImagePath());

    assertEquals(4, gameState.wolken().size());

    assertEquals(2, gameState.bienen().size());

    assertEquals(0, gameState.stiche());

    assertEquals(new Vertex(200, 200), gameState.spieler().getPosition());
    assertEquals(Vertex.ZERO, gameState.spieler().getVelocity());
  }

  @Test
  void stateGetsModified() {
    List<Vertex> enemyPositionsBeforeMove = gameState.bienen().stream()
                                                .map(GameObject::getPosition).collect(Collectors.toList());
    assertAll(gameState.bienen().stream().map(GameObject::getVelocity)
                  .map(vertex -> () -> assertNotEquals(Vertex.ZERO, vertex)));
    assertEquals(2, enemyPositionsBeforeMove.size());

    gameState = simpleGame.calculateTick(gameState, gameInput, 1);

    var enemyPositionsAfterMove = gameState.bienen().stream()
                                      .map(GameObject::getPosition).collect(Collectors.toList());

    assertEquals(2, enemyPositionsAfterMove.size());

    assertAll(
        enemyPositionsAfterMove.stream()
            .map(vertex -> () -> assertFalse(enemyPositionsBeforeMove.contains(vertex),
                "vertex " + vertex + " is in " + enemyPositionsBeforeMove)));
  }

  @Test
  void isStoppedWorks() {
    assertFalse(gameState.getStatus().isOver());

    gameState = new SimpleGameState(gameState.spieler(),
        gameState.bienen(),
        gameState.wolken(),
        gameState.verlorenText(),
        gameState.sticheText(),
        gameState.hintergrund(),
        10,
        gameState.schadenBekommen());

    assertTrue(gameState.getStatus().isOver());
  }

  @Test
  void gameEndsWhenRunAWhile() {
    assertFalse(gameState.getStatus().isOver());

    var playerPos = gameState.spieler().getPosition();
    gameState.bienen()
        .stream()
        .peek(gameObject -> gameObject.setWidth(20))
        .peek(gameObject -> gameObject.setHeight(20))
        .forEach(gameObject -> gameObject.setPosition(playerPos));

    gameState = simpleGame.calculateTick(gameState, gameInput, 1);

    assertEquals(2, gameState.stiche());

    for (int i = 0; i < 2034 && !gameState.getStatus().isOver(); i++) {
      gameState = simpleGame.calculateTick(gameState, gameInput, 1);
    }
    gameState = simpleGame.calculateTick(gameState, gameInput, 1);
    assertTrue(gameState.getStatus().isOver());
  }
}
