package name.panitz.game.example.simple;

import name.panitz.game.framework.input.ButtonStatus;
import name.panitz.game.framework.input.Input;
import name.panitz.game.framework.input.KeyInput;
import name.panitz.game.framework.logic.InputState;

public record SimpleGameInputState(
    boolean leftPressed,
    boolean rightPressed,
    boolean upPressed,
    boolean downPressed
) implements InputState<SimpleGameInputState> {

  public SimpleGameInputState handleInput(Input input) {
    if (input instanceof KeyInput keyInput && keyInput.keyCode() != null) {
      return switch (keyInput.keyCode()) {
        case LEFT_ARROW -> setLeftPressed(keyInput.status() == ButtonStatus.PRESSED);
        case RIGHT_ARROW -> setRightPressed(keyInput.status() == ButtonStatus.PRESSED);
        case UP_ARROW -> setUpPressed(keyInput.status() == ButtonStatus.PRESSED);
        case DOWN_ARROW -> setDownPressed(keyInput.status() == ButtonStatus.PRESSED);
        default -> this;
      };
    }
    return this;
  }

  private SimpleGameInputState setLeftPressed(boolean leftPressed) {
    return new SimpleGameInputState(leftPressed, rightPressed, upPressed, downPressed);
  }

  private SimpleGameInputState setRightPressed(boolean rightPressed) {
    return new SimpleGameInputState(leftPressed, rightPressed, upPressed, downPressed);
  }

  private SimpleGameInputState setUpPressed(boolean upPressed) {
    return new SimpleGameInputState(leftPressed, rightPressed, upPressed, downPressed);
  }

  private SimpleGameInputState setDownPressed(boolean downPressed) {
    return new SimpleGameInputState(leftPressed, rightPressed, upPressed, downPressed);
  }
}
