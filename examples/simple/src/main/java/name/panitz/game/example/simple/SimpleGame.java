package name.panitz.game.example.simple;

import java.util.List;
import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.logic.GameLogic;
import name.panitz.game.framework.object.GameObject;
import name.panitz.game.framework.object.ImageObject;
import name.panitz.game.framework.object.TextObject;
import name.panitz.game.framework.sound.SoundObject;

public class SimpleGame implements GameLogic<SimpleGameState, SimpleGameInputState> {
  public static final String WOLKE_FILE_PATH = "wolke.png";
  public static final String BIENE_FILE_PATH = "biene.png";

  /*+ Für die erfolgten Stiche erfolgt ein kleines Geräusch. */
  SoundObject outch = new SoundObject("crash.wav");

  /*+ Es folgt der Konstruktor, in dem alle Spielfiguren initialisiert werden. */

  /*+ Die steuerbare Spielfigur ist ein Bildobjekt und es wird über die Oberklasse initialisiert. */
  protected GameObject player() {
    return new ImageObject("hexe.png", new Vertex(200, 200), Vertex.ZERO);
  }

  public double getWidth() {
    return 800;
  }

  public double getHeight() {
    return 600;
  }


  /*+ Als nächstes sind die Checks, die nach jedem Tic der Spielschleife durchgeführt werden. */
  @Override
  public SimpleGameState calculateTick(SimpleGameState gameState, SimpleGameInputState inputState, double delta) {
    handleInput(gameState, inputState);

    /*+ Jede Wolke, die außerhalb des Spielfeldes geflogen ist, wird wieder auf die andere Seite des Spielfeldes gesetzt. */
    for (GameObject wolke : gameState.wolken()) {
      resetPosition(wolke);
    }

    int stiche = gameState.stiche();
    SoundObject schadenBekommen = null;
    /*+ Als nächstes werden die Bienen betrachtet.*/
    for (GameObject biene : gameState.bienen()) {
      /*+ Zunächst einmal, ob diese auch außerhalb des Spielfeldes geflogen sind. */
      resetPosition(biene);
      /*+ Dann, ob sie die Spielfigur berühren und damit einen Bienenstich auslösen. */
      if (gameState.spieler().touches(biene)) {
        stiche++;
        gameState.sticheText().text = "Bienenstiche: " + stiche;
        biene.setPosition(new Vertex(getWidth() + 10.0, biene.getPosition().y()));
        schadenBekommen = outch;
      }
    }

    TextObject verlorenText = null;
    /*+ Wir beenden das Spiel nach 10 Stichen. (Dann kommt es wohl definitiv zu einer allergischen Reaktion.)*/
    if (stiche >= 10) {
      verlorenText = new TextObject(new Vertex(100, 300), "Du hast verloren", "Helvetica", 56);
    }

    return new SimpleGameState(
        gameState.spieler(),
        gameState.bienen(),
        gameState.wolken(),
        verlorenText,
        gameState.sticheText(),
        gameState.hintergrund(),
        stiche,
        schadenBekommen);
  }

  protected SimpleGameState handleInput(SimpleGameState state, SimpleGameInputState input) {
    var player = state.spieler();
    if (input.downPressed()) player.accelerate(Vertex.DOWN);
    else if (input.upPressed()) player.accelerate(Vertex.UP);
    if (input.leftPressed()) player.accelerate(Vertex.LEFT);
    else if (input.rightPressed()) player.accelerate(Vertex.RIGHT);
    return state;
  }

  /*+ Eine Hilfsfunktion um Objekte auf die rechte Seite sed Bildschirmes zu schieben.*/
  private void resetPosition(GameObject g) {
    if (g.getPosition().x() + g.getWidth() < 0) {
      g.setPosition(new Vertex(getWidth(), g.getPosition().y()));
    }
  }

  @Override
  public SimpleGameState createInitialState() {
    return new SimpleGameState(
        player(),
        List.of(
            new ImageObject(BIENE_FILE_PATH, new Vertex(800, 100), new Vertex(-1, 0)),
            new ImageObject(BIENE_FILE_PATH, new Vertex(800, 300), new Vertex(-1.5, 0))
        ),
        List.of(
            new ImageObject(WOLKE_FILE_PATH, new Vertex(800, 10), new Vertex(-1, 0)),
            new ImageObject(WOLKE_FILE_PATH, new Vertex(880, 90), new Vertex(-1.2, 0)),
            new ImageObject(WOLKE_FILE_PATH, new Vertex(1080, 60), new Vertex(-1.1, 0)),
            new ImageObject(WOLKE_FILE_PATH, new Vertex(980, 110), new Vertex(-0.9, 0))
        ),
        null,
        new TextObject(new Vertex(30, 35), "Kleines Beispielspiel", "Helvetica", 28),
        new ImageObject("wiese.jpg"),
        0,
        null
    );
  }

  @Override
  public SimpleGameInputState createInitialInput() {
    return new SimpleGameInputState(false, false, false, false);
  }
}

