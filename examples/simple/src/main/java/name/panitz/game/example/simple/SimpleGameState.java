package name.panitz.game.example.simple;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import name.panitz.game.framework.GameStatus;
import name.panitz.game.framework.logic.GameState;
import name.panitz.game.framework.object.GameObject;
import name.panitz.game.framework.object.ImageObject;
import name.panitz.game.framework.object.TextObject;
import name.panitz.game.framework.render.Paintable;
import name.panitz.game.framework.sound.SoundObject;
import name.panitz.game.framework.sound.Soundable;

public record SimpleGameState(
    GameObject spieler,
    List<ImageObject> bienen,
    List<ImageObject> wolken,
    TextObject verlorenText,
    TextObject sticheText,
    ImageObject hintergrund,
    int stiche,
    SoundObject schadenBekommen) implements GameState {

  @Override
  public GameStatus getStatus() {
    return stiche >= 10 ? GameStatus.LOST : GameStatus.RUNNING;
  }

  @Override
  public List<GameObject> getAllGameObjects() {
    LinkedList<GameObject> result = new LinkedList<>();
    result.add(hintergrund);
    result.addAll(wolken);
    result.addAll(bienen);
    result.add(sticheText);
    result.add(spieler);
    if (verlorenText != null) {
      result.add(verlorenText); // verlorenText may be null
    }
    return result;
  }

  @Override
  public List<Paintable> getAllObjectsToPaint() {
    return Collections.unmodifiableList(getAllGameObjects());
  }

  @Override
  public List<Soundable> getAllSoundsToPlay() {
    return schadenBekommen != null ? Collections.singletonList(schadenBekommen) : Collections.emptyList();
  }
}
