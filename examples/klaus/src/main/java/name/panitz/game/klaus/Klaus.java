package name.panitz.game.klaus;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.object.FallingImage;

public class Klaus extends FallingImage {

  public Klaus(Vertex corner) {
    super("player.png", corner, new Vertex(0, 0));
  }
}
