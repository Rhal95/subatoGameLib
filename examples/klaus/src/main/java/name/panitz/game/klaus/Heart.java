package name.panitz.game.klaus;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.object.ImageObject;

public class Heart extends ImageObject {

  public Heart(Vertex corner) {
    super("heart.png", corner, new Vertex(0, 0));
  }

}
