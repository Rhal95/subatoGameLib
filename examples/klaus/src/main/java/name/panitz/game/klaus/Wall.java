package name.panitz.game.klaus;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.object.ImageObject;

public class Wall extends ImageObject {

  public Wall(Vertex corner) {
    super("wall.png", corner, new Vertex(0, 0));
  }

}
