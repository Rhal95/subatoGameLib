package name.panitz.game.klaus;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.object.FallingImage;

public class Barrel extends FallingImage {

  public Barrel(Vertex corner) {
    super("fass.gif", corner, new Vertex(1, 0));
  }

  public void fromTop(double wi) {
    setPosition(new Vertex(Math.random() * (wi - 2 * 40) + 40, -40));
  }
}
