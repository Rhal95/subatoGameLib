package name.panitz.game.klaus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import name.panitz.game.framework.GameStatus;
import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.input.KeyInput;
import name.panitz.game.framework.input.MouseInput;
import name.panitz.game.framework.logic.AbstractGame;
import name.panitz.game.framework.object.FallingImage;
import name.panitz.game.framework.object.GameObject;
import name.panitz.game.framework.object.TextObject;
import name.panitz.game.framework.sound.SoundObject;
import name.panitz.game.framework.sound.Soundable;

public class HeartsOfKlaus extends AbstractGame<HeartsOfKlaus> {
  static final int GRID_WIDTH = 34;
  List<Wall> walls = new ArrayList<>();
  List<Heart> hearts = new ArrayList<>();
  List<Barrel> barrels = new ArrayList<>();
  int energy = 5;
  int collectedHearts = 0;
  Klaus klaus;
  String level1 =
      "w                 f  w\n" +
          "whf         h    www w\n" +
          "wwwww     wwww       w\n" +
          "w        h           w\n" +
          "w       wwww       h w\n" +
          "w                    w\n" +
          "w      hf          fhw\n" +
          "w    wwww       wwwwww\n" +
          "w                    w\n" +
          "wh             h     w\n" +
          "wwwww       wwww     w\n" +
          "w                    w\n" +
          "w p    h            hw\n" +
          "w   wwww        wwwwww\n" +
          "w                    w\n" +
          "w                   gw\n" +
          "wwwwwwww  wwwwwwwwwwww";
  SoundObject crash = new SoundObject("crash.wav");
  List<SoundObject> playSound = new LinkedList<>();

  public HeartsOfKlaus() {
    klaus = new Klaus(new Vertex(0, 0));

    BufferedReader r = new BufferedReader(new StringReader(level1));
    int l = 0;
    try {
      for (String line = r.readLine(); line != null; line = r.readLine()) {
        int col = 0;
        for (char c : line.toCharArray()) {
          switch (c) {
            case 'w':
              walls.add(new Wall
                            (new Vertex(col * GRID_WIDTH, l * GRID_WIDTH)));
              break;
            case 'h':
              hearts.add(new Heart
                             (new Vertex(col * GRID_WIDTH, l * GRID_WIDTH)));
              break;
            case 'f':
              barrels.add(new Barrel(
                  new Vertex(col * GRID_WIDTH, l * GRID_WIDTH)));
              break;
            case 'p':
              klaus.setPosition(new Vertex(col * GRID_WIDTH, l * GRID_WIDTH - 2));
              break;
          }
          col++;
        }
        l++;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public double getWidth() {
    return 22 * GRID_WIDTH;
  }

  @Override
  public double getHeight() {
    return 17 * GRID_WIDTH;
  }

  @Override
  public GameStatus getStatus() {
    if (lost()) {
      return GameStatus.LOST;
    } else if (won()) {
      return GameStatus.WON;
    }
    return GameStatus.RUNNING;
  }

  @Override
  public List<GameObject> getAllGameObjects() {
    var gameObjects = new LinkedList<GameObject>();
    gameObjects.addAll(this.walls);
    gameObjects.addAll(this.barrels);
    gameObjects.addAll(this.hearts);
    gameObjects.add(this.klaus);
    gameObjects.add(new TextObject(new Vertex(50, 10), "Energy: " + energy, "arial", 15));
    gameObjects.add(new TextObject(new Vertex(50, 30), "Hearts: " + hearts.size(), "arial", 15));
    return gameObjects;
  }

  @Override
  public List<Soundable> getAllSoundsToPlay() {
    return List.of();
  }

  private void playerBarrelCollision() {
    for (Barrel p : barrels) {
      if (p.touches(klaus)) {
        energy--;
        playSound.add(crash);
        p.fromTop(getWidth());
      }
      if (p.getPosition().y() > getHeight()) {
        p.fromTop(getWidth());
      }
    }
  }

  private void fallingBarrel() {
    for (FallingImage p : barrels) {
      boolean isStandingOnTop = false;
      for (GameObject wall : walls) {
        if (p.touches(wall)) {
          p.restart();
        }
        if (p.isStandingOnTopOf(wall)) {
          isStandingOnTop = true;
        }
      }
      if (!isStandingOnTop && !p.isJumping) {
        p.startJump(0.1);
      }
    }

  }

  private void checkPlayerWallCollsions() {
    boolean isStandingOnTop = false;
    for (GameObject wall : walls) {
      if (klaus.touches(wall)) {
        klaus.stop();
        return;
      }
      if (klaus.isStandingOnTopOf(wall)) {
        isStandingOnTop = true;
      }
    }

    if (!isStandingOnTop && !klaus.isJumping)
      klaus.startJump(0.1);
  }

  private void collectHearts() {
    Heart removeMe = null;
    for (Heart heart : hearts) {
      if (klaus.touches(heart)) {
        removeMe = heart;
        collectedHearts++;
        break;
      }
    }
    if (removeMe != null)
      hearts.remove(removeMe);
  }

  @Override
  public void calculateTick(double deltaTime) {
    getAllGameObjects().forEach(GameObject::move);
    collectHearts();
    checkPlayerWallCollsions();
    fallingBarrel();
    playerBarrelCollision();
    if (klaus.getPosition().y() > getHeight()) {
      klaus.moveBy(new Vertex(GRID_WIDTH, getHeight() - 80));
    }
  }

  @Override
  public void handleMouseInput(MouseInput input) {
  }

  @Override
  public void handleKeyInput(KeyInput input) {
    if (input != null && input.keyCode() != null)
      switch (input.keyCode()) {
        case LEFT_ARROW:
          klaus.left();
          break;
        case RIGHT_ARROW:
          klaus.right();
          break;
        case UP_ARROW:
          klaus.jump();
          break;
        case DOWN_ARROW:
          klaus.stop();
          break;
        default:
      }
  }

  public boolean lost() {
    return energy <= 0;
  }

  public boolean won() {
    return hearts.isEmpty();
  }

}
