package de.weber.game.clicker;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.object.GameObject;
import name.panitz.game.framework.render.GraphicsTool;

public class Rectangle implements GameObject {
  private final double width;
  private final double height;

  public Rectangle(double width, double height) {
    this.width = width;
    this.height = height;
  }

  @Override
  public double getWidth() {
    return width;
  }

  @Override
  public void setWidth(double w) {
  }

  @Override
  public double getHeight() {
    return height;
  }

  @Override
  public void setHeight(double h) {
  }

  @Override
  public Vertex getVelocity() {
    return Vertex.ZERO;
  }

  @Override
  public void setVelocity(Vertex v) {

  }

  @Override
  public void setPosition(Vertex position) {

  }

  @Override
  public Vertex getPosition() {
    return Vertex.ZERO;
  }

  @Override
  public void paintTo(GraphicsTool g) {

  }
}
