package de.weber.game.clicker;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.object.AbstractGameObject;
import name.panitz.game.framework.render.GraphicsTool;

public class Cursor extends AbstractGameObject {
  public Cursor(double widthAndHeight) {
    super(widthAndHeight, widthAndHeight, Vertex.ZERO);
  }

  @Override
  public void paintTo(GraphicsTool g) {
    g.setColor(0, 0.5, 0);
    var halfWidth = new Vertex(getWidth() / 2, getHeight() / 2);
    var position = getPosition().add(halfWidth.mult(-1));
    g.drawCircle(position.x(), position.y(), getWidth());
    g.drawLine(position.add(halfWidth).x(), position.y(),
        position.add(halfWidth).x(), position.add(halfWidth.mult(2)).y());
    g.drawLine(position.x(), position.add(halfWidth).y(),
        position.add(halfWidth.mult(2)).x(), position.add(halfWidth).y());
  }
}
