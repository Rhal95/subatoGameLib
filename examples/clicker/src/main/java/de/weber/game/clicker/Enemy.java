package de.weber.game.clicker;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.object.AbstractGameObject;
import name.panitz.game.framework.render.GraphicsTool;

public class Enemy extends AbstractGameObject {
  protected Enemy(double width, double height, Vertex position, Vertex velocity) {
    super(width, height, position, velocity);
  }

  protected Enemy(double width, double height, Vertex position) {
    super(width, height, position);
  }

  protected Enemy(double width, double height) {
    super(width, height);
  }

  protected Enemy(double widthAndHeight) {
    super(widthAndHeight);
  }

  @Override
  public void paintTo(GraphicsTool g) {

  }
}
