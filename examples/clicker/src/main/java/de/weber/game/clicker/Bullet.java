package de.weber.game.clicker;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.object.AbstractGameObject;
import name.panitz.game.framework.render.GraphicsTool;

public class Bullet extends AbstractGameObject {
  public Bullet(Vertex position, Vertex direction) {
    super(15, 15, position, direction.norm().mult(5));
  }

  @Override
  public void paintTo(GraphicsTool g) {
    var halfWidth = new Vertex(getWidth(), getHeight()).mult(0.5);
    var position = getPosition().add(halfWidth.mult(-1));
    g.setColor(0, 0, 0);
    g.fillCircle(position.x(), position.y(), getWidth());
  }
}
