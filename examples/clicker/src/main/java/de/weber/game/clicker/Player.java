package de.weber.game.clicker;

import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.object.AbstractGameObject;
import name.panitz.game.framework.render.GraphicsTool;

public class Player extends AbstractGameObject {
  double rotation = 0;

  protected Player(double width, double height, Vertex position, Vertex velocity) {
    super(width, height, position, velocity);
  }

  protected Player(double width, double height, Vertex position) {
    super(width, height, position);
  }

  protected Player(double width, double height) {
    super(width, height);
  }

  protected Player(double widthAndHeight) {
    super(widthAndHeight);
  }

  @Override
  public void paintTo(GraphicsTool g) {
    var halfWidth = new Vertex(getWidth() / 2, getHeight() / 2);
    var position = getPosition().add(halfWidth.mult(-1));
    g.setColor(0.8, 0.3, 0.3);
    g.fillCircle(position.x(), position.y(), getHeight());
    g.setColor(0, 0, 0);
    var v = new Vertex(Math.cos(rotation), Math.sin(rotation));
    Vertex direction = v.mult(20);

    var secondPosition = getPosition().add(direction);
    g.drawLine(getPosition().x(), getPosition().y(), secondPosition.x(), secondPosition.y());
    g.setColor(1, 1, 1);
  }
}
