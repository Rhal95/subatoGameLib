package de.weber.game.clicker;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import name.panitz.game.framework.GameStatus;
import name.panitz.game.framework.Vertex;
import name.panitz.game.framework.input.ButtonCode;
import name.panitz.game.framework.input.ButtonStatus;
import name.panitz.game.framework.input.Input;
import name.panitz.game.framework.input.MouseInput;
import name.panitz.game.framework.logic.GameLogic;
import name.panitz.game.framework.logic.GameState;
import name.panitz.game.framework.logic.InputState;
import name.panitz.game.framework.object.GameObject;
import name.panitz.game.framework.render.Paintable;
import name.panitz.game.framework.sound.Soundable;

public class ClickerGame implements GameLogic<ClickerGame, ClickerGame>, GameState, InputState<ClickerGame> {

  int score;
  List<Enemy> enemies;
  Player player;
  Cursor cursor;
  List<Bullet> bullets;
  Vertex cursorPosition = Vertex.ZERO;
  boolean clicked;

  @Override
  public ClickerGame calculateTick(ClickerGame gameState, ClickerGame input, double deltaTime) {
    var cursor = input.cursorPosition;
    gameState.cursor.setPosition(cursor);
    var player = gameState.player;
    var direction = cursor.add(player.getPosition().mult(-1));
    player.rotation = Math.atan2(direction.y(), direction.x());
    if (input.clicked) {
      System.out.println("Spawn bullet");
      gameState.bullets.add(new Bullet(player.getPosition(), direction));
      input.clicked = false;
    }
    var screen = new Rectangle(getWidth(), getHeight());
    gameState.bullets = gameState.bullets.stream().filter(bullet -> bullet.touches(screen)).collect(Collectors.toList());
    gameState.getAllGameObjects().forEach(GameObject::move);
    return gameState;
  }

  @Override
  public ClickerGame createInitialState() {
    this.enemies = new LinkedList<>();
    this.cursor = new Cursor(25);
    this.bullets = new LinkedList<>();
    this.player = new Player(30, 30, new Vertex(400, 300));
    this.score = 10;
    return this;
  }

  @Override
  public ClickerGame createInitialInput() {
    this.cursorPosition = Vertex.ZERO;
    this.clicked = false;
    return this;
  }

  @Override
  public double getHeight() {
    return 600;
  }

  @Override
  public double getWidth() {
    return 800;
  }

  @Override
  public GameStatus getStatus() {
    return score > 0 ? GameStatus.RUNNING : GameStatus.LOST;
  }

  @Override
  public List<GameObject> getAllGameObjects() {
    List<GameObject> gameObjectList = new LinkedList<>();
    gameObjectList.addAll(enemies);
    gameObjectList.addAll(bullets);
    gameObjectList.add(player);
    gameObjectList.add(cursor);
    return gameObjectList;
  }

  @Override
  public List<Paintable> getAllObjectsToPaint() {
    return Collections.unmodifiableList(getAllGameObjects());
  }

  @Override
  public List<Soundable> getAllSoundsToPlay() {
    return List.of();
  }

  @Override
  public ClickerGame handleInput(Input input) {
    if (input instanceof MouseInput mouseInput) {
      cursorPosition = mouseInput.position();
      if (mouseInput.mouseButton() == ButtonCode.LEFT_MOUSE && mouseInput.status() == ButtonStatus.PRESSED) {
        clicked = true;
      }
    }
    return this;
  }
}
