package name.panitz.game.starter;

import de.weber.game.clicker.ClickerGame;
import name.panitz.game.framework.GameExecutor;
import name.panitz.game.framework.clock.SimpleClock;
import name.panitz.game.framework.libgdx.lwjgl.LwjglGame;
import name.panitz.game.framework.logic.GameLogic;
import name.panitz.game.framework.sound.SoundTool;

public class PlayLwjgl {
  public static void main(String[] args) {
      var gameScreen = new LwjglGame<ClickerGame>();
      SimpleClock clock = SimpleClock.FAST_CLOCK;
      GameLogic<ClickerGame, ClickerGame> gameLogic = new ClickerGame();
      var executor = new GameExecutor<>(gameLogic, gameScreen, gameScreen, clock, SoundTool.NO_SOUND);
      while (!executor.isStopped()) {
          executor.playGame();
      }
  }
}
