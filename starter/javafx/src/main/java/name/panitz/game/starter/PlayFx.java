package name.panitz.game.starter;

import name.panitz.game.framework.GameExecutor;
import name.panitz.game.framework.clock.SimpleClock;
import name.panitz.game.framework.fx.GameApplication;
import name.panitz.game.framework.logic.GameLogic;
import name.panitz.game.framework.logic.GameState;
import name.panitz.game.framework.logic.InputState;
import name.panitz.game.framework.sound.SoundTool;
import name.panitz.game.klaus.HeartsOfKlaus;

public class PlayFx {

  static <L extends GameLogic<S, I>, S extends GameState, I extends InputState<I>> GameExecutor<S, I> executor(L gameLogic) {
    var gameScreen = GameApplication.<I>startFxGame();
    SimpleClock clock = SimpleClock.FAST_CLOCK;
    return new GameExecutor<>(gameLogic, gameScreen, gameScreen, clock, SoundTool.NO_SOUND);
  }

  public static void main(String[] args) {
    GameLogic<HeartsOfKlaus, HeartsOfKlaus> gameLogic = new HeartsOfKlaus();
    var executor = executor(gameLogic);
    while (!executor.isStopped()) {
      executor.playGame();
    }
  }
}
