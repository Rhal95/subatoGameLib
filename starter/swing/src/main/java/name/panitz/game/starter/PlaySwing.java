package name.panitz.game.starter;

import name.panitz.game.example.simple.SimpleGame;
import name.panitz.game.example.simple.SimpleGameInputState;
import name.panitz.game.framework.GameExecutor;
import name.panitz.game.framework.clock.SimpleClock;
import name.panitz.game.framework.swing.JavaSoundTool;
import name.panitz.game.framework.swing.SwingGame;

public class PlaySwing {

    public static void main(String[] args) {
        var gameScreen = new SwingGame<SimpleGameInputState>();
        SimpleClock clock = SimpleClock.FAST_CLOCK;
        var gameLogic = new SimpleGame();
        var executor = new GameExecutor<>(gameLogic, gameScreen, gameScreen, clock, new JavaSoundTool());
        while (!executor.isStopped()) {
            executor.playGame();
        }
    }
}
